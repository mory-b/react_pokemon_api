import React from "react";
import { Card, Col, Row } from "react-bootstrap";

function PokemonView(props) {
    function getStats(stats) {
        return stats.reverse().map((stat, i) => {
            return (
                <Col key={i} xs={6} sm={6} md={6} lg={6} xl={6} as="div">
                    {stat.stat.name} : {stat.base_stat}
                </Col>
            );
        });
    }

    function getStats(stats) {

        return stats.reverse().map((stat, i) => {
            return (
                <Col key={i} xs={6} sm={6} md={6} lg={6} xl={6} as="div">
                    {stat.stat.name} : {stat.base_stat}
                </Col>
            );
        });
    }

    function getGames(games) {
        let game_array = [];
        for (const game of games.reverse()) {
            game_array.push(game.version.name);
        }

        return game_array.join(", ");
    }

    return (
        <Col xs={12} sm={6} md={6} lg={6} xl={4} className="mb-2">
            <Card>
                <Card.Img src={props.pokemon.image} />
                <Card.Body>
                    <Card.Title className="text-center mb-1">
                        {props.pokemon.species.name}
                    </Card.Title>
                    <Card.Subtitle className="mb-1">Stats</Card.Subtitle>
                    <Row style={{ fontSize: 13 }} className="mb-3">
                        {getStats(props.pokemon.stats)}
                    </Row>
                    <Card.Subtitle>Games</Card.Subtitle>
                    <Card.Text style={{ fontSize: 13 }}>
                        {getGames(props.pokemon.game_indices)}
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    );
}

export default PokemonView;
