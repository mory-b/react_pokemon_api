import React from "react";
import Menu from "./Menu";
import PokemonApp from "./PokemonApp";


function App() {
    return (
        <div>
            <Menu />
            <PokemonApp />
        </div>
    );
}

export default App;
