import React, { useState } from "react";
import ListPokemon from "./ListPokemon";
import FormPokemon from "./FormPokemon";
import { Container } from "react-bootstrap";


function PokemonApp() {

    const [search, setSearch] = useState({ type: '' });

    function searchPokemon(type, language) {
        setSearch({
            type
        });
    }

    return (
        <Container>
            <FormPokemon onFormSubmit={searchPokemon} />
            <ListPokemon search={search} />
        </Container>
    );
}

export default PokemonApp;