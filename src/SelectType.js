import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";


function SelectType(props) {

    const [value, setValue] = useState('');

    useEffect(() => {
        props.onSelectChange(value);
    });

    function handleChange(event) {
        setValue(event.target.value);
    }

    return (
        <Form.Group controlId={props.id}>
            <Form.Label>{props.label}</Form.Label>
            <Form.Control as="select" onChange={handleChange} multiple={props.isMultiple}>
                {props.elements.map((element, index) => {
                    return (
                        <option key={index} value={props.values[index]}>
                            {element}
                        </option>
                    );
                })}
            </Form.Control>
        </Form.Group>
    );

}

export default SelectType;
