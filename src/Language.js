import React from "react";
import SelectType from "./SelectType";

function Language(props) {

    const language = ["Français / French", "Anglais / English"];
    const values = ["fra", "eng"];

    function handleLanguageChange(value) {
        props.onLanguageChange(value);
    }

    return (
        <SelectType
            elements={language}
            isMultiple={false}
            label="Choisir une langue / Choose language"
            onSelectChange={handleLanguageChange}
            values={values}
        />
    );

}


export default Language;