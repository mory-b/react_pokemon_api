import React, { useState, useEffect } from "react";
import axios from "axios";
import PokemonView from "./PokemonView";
import { Row, Pagination } from "react-bootstrap";

function ListPokemon(props) {
    let items = [];

    useEffect(() => {
        fetchPokemonsByType(props.search.type);
        return () => {
            setPokemons([]);
            setPokemonUrl([]);
        };
    }, [props.search]);

    const [pokemons_url, setPokemonUrl] = useState([]);

    useEffect(() => {
        if (pokemons_url.length > 0) {
            fetchPokemon(pokemons_url);
        }
    }, [pokemons_url]);

    const [pokemons, setPokemons] = useState([]);
    const [pokemons_paginate, setPokemonsPaginate] = useState([]);
    const [active, setActive] = useState(1)

    useEffect(() => {
        setPokemonsPaginate(pokemons.slice(0, 20));
    }, [pokemons]);

    function handlePokemonPaginate(e) {

        let indice = parseInt(e.target.text);
        let indice_depart = indice - 1;
        let pokemon_array = pokemons.slice((indice_depart * 20), ((indice_depart * 20) + 20));
        setActive(indice);
        setPokemonsPaginate(pokemon_array);
    }

    function fetchPokemonsByType(type) {
        if (props.search.type.length > 0) {
            if (sessionStorage.getItem([type])) {
                setPokemonUrl(JSON.parse(sessionStorage.getItem([type])));
            } else {
                axios({
                    method: "get",
                    url: "/type/" + type,
                    baseURL: "https://pokeapi.co/api/v2/"
                }).then(function (response) {
                    let pokemon_array = [];
                    response.data.pokemon.map(pokemon => {
                        return pokemon_array.push(pokemon.pokemon.url);
                    });
                    sessionStorage.setItem(
                        [type],
                        JSON.stringify(pokemon_array)
                    );
                    setPokemonUrl(pokemon_array);
                });
            }
        }
    }

    async function fetchPokemon(pokemon_url) {
        let pokemon_array = [];
        for (const url of pokemon_url) {
            if (sessionStorage.getItem(url)) {
                pokemon_array.push(JSON.parse(sessionStorage.getItem(url)));
            } else {

                const response = await axios(url);
                let data = response.data;
                let pokemon = {
                    image: data.sprites.front_default,
                    species: data.species,
                    game_indices: data.game_indices,
                    stats: data.stats
                };
                sessionStorage.setItem([url], JSON.stringify(pokemon));
                pokemon_array.push(pokemon);
            }
        }
        setPokemons(pokemon_array);
        setActive(1);
    }

    if (props.search.type.length === 0) {
        return null;
    }

    for (let number = 1; number <= Math.ceil(pokemons.length / 20); number++) {
        items.push(
            <Pagination.Item key={number} active={number === active}>
                {number}
            </Pagination.Item>
        );
    }

    return (
        <div>
            <h1 className="text-center mb-3">
                List of pokemon : {props.search.type}
            </h1>
            <Row>
                {pokemons_paginate.map((pokemon, id) => {
                    return <PokemonView key={id} pokemon={pokemon} />
                })}
            </Row>
            <div>
                <Pagination onClick={handlePokemonPaginate}>
                    {items}
                </Pagination>
            </div>
        </div >
    );
}

export default ListPokemon;
