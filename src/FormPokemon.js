import React from "react";
import { Form, Button } from "react-bootstrap";
import Type from "./Type";
// import Language from "./Language";


function FormPokemon(props) {

    // let language = '';
    let type = '';

    function handleTypeChange(value) {
        type = value;
    }

    // function handleLanguageChange(value) {
    //     language = value;
    // }

    function handleSubmit(event) {

        if (type.length > 0) {
            props.onFormSubmit(type)
        }
        event.preventDefault();
    }

    return (
        <Form onSubmit={handleSubmit}>
            <Type onTypeChange={handleTypeChange} />
            {/* <Language onLanguageChange={handleLanguageChange} /> */}
            <Button variant="primary" type="submit">
                Recherche
                </Button>
        </Form>
    )
}

export default FormPokemon;
