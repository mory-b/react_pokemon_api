import React from "react";
import { Navbar } from "react-bootstrap";

function Menu() {

    return (
        <Navbar bg="dark" expand="lg" className="mb-5" >
            <Navbar.Brand>Pokemon</Navbar.Brand>
        </Navbar>
    );
}

export default Menu;
