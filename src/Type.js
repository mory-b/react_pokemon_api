import React, { useState, useEffect } from "react";
import axios from "axios";
import SelectType from "./SelectType";

function Type(props) {

    const [types, setTypes] = useState([]);

    useEffect(() => {
        fetchType(types);
    }, [types])

    function handleTypeChange(value) {
        props.onTypeChange(value);
    }

    function fetchType() {

        axios({
            method: "get",
            url: "/type",
            baseURL: "https://pokeapi.co/api/v2/"
        }).then(function (response) {
            let types_array = [];

            response.data.results.map((result) => {
                return types_array.push(result.name);

            });

            setTypes(types_array);
        });
    }

    if (types.length === 0) {
        return null;
    }

    return (
        <SelectType
            elements={types}
            isMultiple={false}
            label="Type de pokémon / Type of pokemon"
            onSelectChange={handleTypeChange}
            values={types}
        />
    );
}

export default Type;
